This repository is a Unity project that contains three procedural content creation techniques for creating dungeons.

The techniques used in the project are:

* **Cellular Automata** - http://youtu.be/sFP8TuAt_Jc?list=UU4i6i-HBjQzBC_pC7l8GB1A

* **Binary Space Partitioning** - http://youtu.be/AUJx3xYM4n4?list=UU4i6i-HBjQzBC_pC7l8GB1A

* **Delaunay Triangulation** - http://youtu.be/CaI6edoGbFY?list=UU4i6i-HBjQzBC_pC7l8GB1A

**Running project**

To run the techniques open the project in Unity. There are three different scenes inside the project, one for each generation technique. Open the scene you wish to run and then run the project from the editor. These techniques have not been set up for use in game so you will have to view them in the scene view.

The Delaunay technique requires the user to click the scene window after the connection graph has been generated for the process to continue.

**Background**

The contents of this repository was made as part of my final year project at University which was an investigation into techniques used to procedurally generate dungeon structures. The corresponding report to this project can be viewed here:

http://www.nathanmwilliams.com/files/AnInvestigationIntoDungeonGeneration.pdf

The report contains description of the development process as well as all reference material that was used while developing the generation techniques, as such it may be helpful to anyone trying to implement similar techniques.

**Contact**

For any further questions or comments I can be contacted at nathMWilliams (at) gmail (.) com or to see other examples of my work visit www.nathanmwilliams.com